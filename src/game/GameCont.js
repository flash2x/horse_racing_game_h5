AnnieRoot.game=AnnieRoot.game||{};
game.GameCont=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*/s.behind_view=null;s.front_view=null;s.gundongtiao=null;s.km_pai=null;s.menu_list=null;s.mutou_box=null;s.mutou_box1=null;s.paodao=null;s.qipaoxian=null;s.reciprocal=null;s.second_horse=null;s.stop_game=null;s.third_horse=null;s.time_list=null;s.user=null;/*_a2x_need_end*/
	annie.initRes(s,"game","GameCont");

	s.time = 60;
	s.timeout;
	annie.kmCont = 0;
	// var fallDown_music = new annie.Sound("resource/Error_sound.mp3");
	// var mutouCont = new game.mutou();
	var all_play = true;
	var gundongtiao = true;
	var gundongtiao_go = true;
	var click = true;
	var now_currentFrame = 2;
	var frontview_speed = 18;
	var behindview_speed = 14;
	var gundongtiao_speed = 25;
	var userhorse_speed = 0.22;
	var com1horse_speed = -0.38;
	var com2horse_speed = -0.2;
	var km_speed = 0.54;
	var stop_time = null;
	var view_id = Math.ceil(Math.random() * 3);
	console.log(view_id);
	s.behind_view.gotoAndStop(view_id);
	s.front_view.gotoAndStop(view_id);
	if (view_id == 0) {
		s.paodao.gotoAndStop(1);
	} else {
		s.paodao.gotoAndStop(2);
	}
	s.addEventListener(annie.Event.ADD_TO_STAGE, function (e) {
		s.user.horse_state.visible = false;
	});
	s.stop_game.addEventListener(annie.MouseEvent.CLICK, function (e) {
		if (all_play && gundongtiao_go && click) {
			click = false;
			gundongtiao_go = false;
			if(!stop_time){
				stop_time =new annie.Timer(3000,1);
				stop_time.addEventListener(annie.Event.TIMER,function (e) {
					gundongtiao_go = true;
					s.user.horse_state.visible = false;
					s.gundongtiao.x = 220;
					s.gundongtiao.y = 856.2;
					km_speed = 0.54;
					com1horse_speed = -0.38;
					com2horse_speed = -0.2;
					userhorse_speed = 0.22;
					frontview_speed = 18;
					behindview_speed = 14;
					now_currentFrame = 2;
					s.user.gotoAndStop(now_currentFrame);
					click = true;
				});
				stop_time.addEventListener(annie.Event.TIMER_COMPLETE, function (e) {
					e.target.reset();
				});
				stop_time.start();
			}else {
				stop_time.start();
			}
			/*3秒后回到初始速度*/
			if (s.gundongtiao.x < 578 && s.gundongtiao.x >= 386) {
				s.user.horse_state.gotoAndStop(1);
				s.user.horse_state.visible = true;
				km_speed = 1.14;
				com1horse_speed = 0.35;
				com2horse_speed = 0.2;
				userhorse_speed = -0.25;
				frontview_speed = 22;
				behindview_speed = 18;
				now_currentFrame = 3;
				s.user.gotoAndStop(now_currentFrame);
				/*加速*/
			} else if (s.gundongtiao.x >= 210 && s.gundongtiao.x < 386) {
				s.user.horse_state.gotoAndStop(2);
				s.user.horse_state.visible = true;
				km_speed = 0.3;
				com1horse_speed = -0.38;
				com2horse_speed = -0.20;
				userhorse_speed = 0.25;
				frontview_speed = 14;
				behindview_speed = 10;
				now_currentFrame = 5;
				s.user.gotoAndStop(now_currentFrame);
				/*减速*/
			} else if (s.gundongtiao.x >= 578 && s.gundongtiao.x < 680) {
				s.user.horse_state.gotoAndStop(1);
				s.user.horse_state.visible = true;
				km_speed = 1.5;
				com1horse_speed = 0.48;
				com2horse_speed = 0.25;
				userhorse_speed = -0.35;
				frontview_speed = 26;
				behindview_speed = 22;
				now_currentFrame = 4;
				s.user.gotoAndStop(now_currentFrame);
				/*超级加速*/
			}
			/*三种速度情况*/
		}
	});
	s.addEventListener(annie.Event.ENTER_FRAME, function (e) {
		if (s.isRuning) {
			if (s.time <= 0) {
				// clearInterval(s.timeout);
				s.gTimer.reset();//重设游戏时间
				s.user.gotoAndStop(1);
				s.second_horse.gotoAndStop(1);
				s.third_horse.gotoAndStop(1);
				annie.globalDispatcher.dispatchEvent("onShowAlert", {alr: "result"});
				s.isRuning = false;
				all_play = false;
			}
			if (all_play) {
				s.second_horse.x -= com1horse_speed;
				s.third_horse.x -= com2horse_speed;
				s.user.x -= userhorse_speed;
				if (s.user.x >= 725) {
					s.user.x = 670;
				}
				if (s.second_horse.x >= 725) {
					s.second_horse.x = 685;
				}
				if (s.third_horse.x >= 725) {
					s.third_horse.x = 650;
				}
				if (s.user.x <= 140) {
					s.user.x = 175;
				}
				if (s.second_horse.x <= 140) {
					s.second_horse.x = 175;
				}
				if (s.third_horse.x <= 140) {
					s.third_horse.x = 175;
				}
				if (view_id == 1) {
					s.front_view.f1.x -= frontview_speed;
					if (s.front_view.f1.x <= -890) {
						s.front_view.f1.x = 0;
						s.front_view.f1.y = -142.50;
					}
					/*前面树木循环*/
					s.behind_view.b1.x -= behindview_speed;
					if (s.behind_view.b1.x <= -2226) {
						s.behind_view.b1.x = -1196;
						s.behind_view.b1.y = -269.5;
					}
					/*后面场景循环*/
				} else if (view_id == 2) {
					s.front_view.f2.x -= frontview_speed;
					if (s.front_view.f2.x <= -869.5) {
						s.front_view.f2.x = -2.5;
						s.front_view.f2.y = -141.65;
					}
					/*前面树木循环*/
					s.behind_view.b2.x -= behindview_speed;
					if (s.behind_view.b2.x <= -2338.8) {
						s.behind_view.b2.x = -1039.5;
						s.behind_view.b2.y = -269.35;
					}
					/*后面场景循环*/
				} else if (view_id == 3) {
					s.front_view.f3.x -= frontview_speed;
					if (s.front_view.f3.x <= -907.3) {
						s.front_view.f3.x = 1.95;
						s.front_view.f3.y = -170.85;
					}
					/*前面树木循环*/
					s.behind_view.b3.x -= behindview_speed;
					if (s.behind_view.b3.x <= -2281) {
						s.behind_view.b3.x = -1034.1;
						s.behind_view.b3.y = -267.95;
					}
					/*后面场景循环*/
				}
				if (s.gundongtiao.x >= 670) {
					s.gundongtiao.x = 670;
					gundongtiao = false;
				} else if (s.gundongtiao.x <= 220) {
					s.gundongtiao.x = 220;
					gundongtiao = true;
				}
				if (gundongtiao_go) {
					if (gundongtiao) {
						s.gundongtiao.x += gundongtiao_speed;
					} else {
						s.gundongtiao.x -= gundongtiao_speed;
					}
				}
				/*速度状态滚动条来回滚动*/
				if (annie.kmCont >= 1200 && annie.kmCont < 1400) {
					annie.kmCont = 1400;
				}
				if (annie.kmCont >= 1600 && annie.kmCont < 1800) {
					annie.kmCont = 1800;
				}
				annie.kmCont += km_speed;
				s.km_pai.km.text = Math.floor(annie.kmCont);
				/*距离增加*/
			}
		}
	});


};
A2xExtend(game.GameCont,annie.Sprite);
game.GameCont.prototype.gTimer = null;
game.GameCont.prototype.start = function () {
	var s = this;
	var action = new annie.Sound("./resource/Dao_bgm.mp3");
	action.play(0, 1);
	s.reciprocal.gotoAndPlay(2);
	s.reciprocal.addEventListener(annie.Event.END_FRAME, function (e) {
		s.reciprocal.visible = false;
		s.addChild(s.handCont);
		annie.Tween.to(s.third_horse, 2, {x: 320});
		annie.Tween.to(s.second_horse, 2, {x: 320});
		annie.Tween.to(s.user, 2, {x: 320});
		annie.Tween.to(s.qipaoxian, 1.5, {x: -100});
		/*出发后起跑线后移，三匹马移动到不同位置*/
		if (s.qipaoxian.x == -100) {
			s.removeChild(s.qipaoxian);
		}
		/*起跑线移出屏幕remove掉*/
		s.user.gotoAndStop(2);
		s.second_horse.gotoAndStop(2);
		s.third_horse.gotoAndStop(2);
		/*用引擎自带的Timer类，实现倒计时功能,修复手机待机黑屏，计算不准出现负数的情况*/
		if(!s.gTimer){
			s.gTimer=new annie.Timer(1000);
			s.gTimer.addEventListener(annie.Event.TIMER,s.gt=function (e) {
				s.time -= 1;
				s.time_list.time_text.text = s.time;
			});
			s.gTimer.start();
		}
		// s.timeout = setInterval(function (e) {
		//     s.time -= 1;
		//     s.time_list.time_text.text = s.time;
		// }, 1000);
		/*倒计时*/
		s.isRuning = true;
	})

}

