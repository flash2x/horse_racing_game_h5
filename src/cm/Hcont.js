AnnieRoot.cm=AnnieRoot.cm||{};
cm.Hcont=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*/s.alert_cm=null;s.con_cm=null;/*_a2x_need_end*/
	annie.initRes(s,"cm","Hcont");
	annie.bg_music=new annie.Sound("./resource/Bg_sound.mp3");
	var homeCont = new home.Homecont();
	var gameCont = null;
	s.con_cm.addChild(homeCont);
	annie.globalDispatcher.addEventListener("onChangeContent", function (e) {
		s.alert_cm.removeAllChildren();
		switch (e.data.cont) {
			case "rule_star":
				gameCont.start();
				break;
			case "back_home":
				s.con_cm.removeAllChildren();
				s.con_cm.addChild(homeCont);
				break;
			case "rule":
				var ruleCont = new rule.RuleCont();
				gameCont =new game.GameCont();
				s.con_cm.addChild(gameCont);
				s.alert_cm.addChild(ruleCont);
				break;
			case "again":
				s.con_cm.removeAllChildren();
				gameCont =new game.GameCont();
				s.con_cm.addChild(gameCont);
				gameCont.start();
		}
	});
	annie.globalDispatcher.addEventListener("onShowAlert", function (e) {
		s.alert_cm.removeAllChildren();
		switch (e.data.alr) {
			case "del_rule":
				break;
			case "result":
				var resultCont = new list.Result();
				s.alert_cm.addChild(resultCont);
				break;
		}
	});
};
A2xExtend(cm.Hcont,annie.Sprite);
