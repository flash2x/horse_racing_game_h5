AnnieRoot.rule=AnnieRoot.rule||{};
rule.RuleCont=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*/s.rule_star=null;/*_a2x_need_end*/
	annie.initRes(s,"rule","RuleCont");
	s.rule_star.addEventListener(annie.MouseEvent.CLICK,function (e) {
		s.play(false);
	});
	s.addEventListener(annie.Event.END_FRAME,function (e) {
		if(e.data.frameIndex == 1){
			annie.globalDispatcher.dispatchEvent("onChangeContent",{cont:"rule_star"});
		}
	});
};
A2xExtend(rule.RuleCont,annie.MovieClip);
