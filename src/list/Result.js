AnnieRoot.list=AnnieRoot.list||{};
list.Result=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*/s.again=null;s.back_home=null;s.jiantou=null;s.km=null;/*_a2x_need_end*/
	annie.initRes(s,"list","Result");
	var go;
	var kmNum = Math.floor(annie.kmCont);
	s.km.km_text.text = kmNum + "米";
	s.back_home.addEventListener(annie.MouseEvent.CLICK, function (e) {
		s.play(false);
		go = 1;
	});
	s.again.addEventListener(annie.MouseEvent.CLICK, function (e) {
		s.play(false);
		go = 0;
	});
	s.addEventListener(annie.Event.END_FRAME, function (e) {
		if(e.data.frameIndex == 1) {
			if(go==1){
				annie.globalDispatcher.dispatchEvent("onChangeContent", {cont: "back_home"});
			}
			else{
				annie.globalDispatcher.dispatchEvent("onChangeContent", {cont: "again"});
			}
		}
	});


	if (kmNum <= 1000) {
		s.jiantou.gotoAndStop(5);
	} else if (kmNum > 1000 && kmNum <= 1200) {
		s.jiantou.gotoAndStop(4);
	} else if (kmNum >= 1400 && kmNum <= 1650) {
		s.jiantou.gotoAndStop(3);
	} else if (kmNum >= 1800 && kmNum < 2400) {
		s.jiantou.gotoAndStop(2);
	} else if (kmNum >= 2400) {
		s.jiantou.gotoAndStop(1);
	}
};
A2xExtend(list.Result,annie.MovieClip);
