AnnieRoot.home=AnnieRoot.home||{};
home.Homecont=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*/s.begin=null;/*_a2x_need_end*/
	annie.initRes(s,"home","Homecont");
	s.begin.addEventListener(annie.MouseEvent.CLICK,function (e) {
		s.play(false);
		annie.bg_music.play(0,9999);
	});
	s.addEventListener(annie.Event.END_FRAME,function (e) {
		if(e.data.frameIndex == 1){
			annie.globalDispatcher.dispatchEvent("onChangeContent",{cont:"rule"});
		}
	});
};
A2xExtend(home.Homecont,annie.MovieClip);
